import Link from 'next/link'
import axios from 'axios'

const BlogPost = ({ post }) => {
    return (
       <div className="section">
           <div>
            {/* <Link href="/" as="/"> <a>Home</a>  </Link> */}
            <Link href="/"><a>Home</a></Link>
           </div>
            <div className="container">
               

                <div className="main_image">
                    <img src={`https://api.dialadelivery.co.ke/uploads/blog/${post.image}.jpeg`}></img>
                </div>
                <h4> {post.title} </h4>
                <div> {post.meta} </div>
            </div>
            <style jsx >
                {`
                
                .section: {
                    text-align: center;
                    padding: 20px;
                    display: block;
                    width: 100%;
                }
                
                .container{
                    width: 50%;
                    margin: 40px auto;
                }
                
                
                `}
            </style>
       </div>
    )
}

export const getStaticProps = async ctx => {
   try {
    const url = ctx.params.url
    const { data } = await axios.get('https://dialadelivery.co.ke/api/blog/'+url)
    return { props: { post: data.article } } 
   } catch (err) {
       throw(err)
   }

   
}

export const getStaticPaths = async () => {

    try {
        const res = await axios.get('https://dialadelivery.co.ke/api/blog')
        console.log('SP Len', res.data.articles.length)
        if (!res.data.state) {
            throw("Server error")
        }
        const paths = res.data.articles.map(post => {
            return { params: { url: post.url } }
        })
        return {
            fallback: false,
            paths: paths
        }

    } catch (err) {
        console.log('error ins gsp')
        throw(err.msg)
    }
}

export default BlogPost