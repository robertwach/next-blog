import Head from 'next/head'
import Link from 'next/link'
import axios from 'axios'

const Home = ( { posts } ) => {

  const postList = () => {
    return posts.articles.map((post) => {
      return (
        <div  key={`${post._id}`}>
          <Link href="/blog/[url]" as={`/blog/${post.url}`} >
            <a>{post.title} </a>
          </Link>
        </div>
      )
    })
  }

  return (
    <div className="container">
      <Head>
        <title> Blazing - Drinks Blog </title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <ul>
          <li>
            <Link href="/about"><a>About</a></Link>
          </li>
        </ul>
        <h1 className="title">
          A simple blog
        </h1>

        <div>
          {postList()}
        </div>

       
      </main>

      <style jsx>{`
        .container {
          padding: 10px;
        }

      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }
      `}</style>
    </div>
  )
}

export const getStaticProps = async ctx => {
 try {
  const { data } = await axios.get('https://dialadelivery.co.ke/api/blog')
  return { props: { posts: data } }
 } catch (err) {
    throw(err)
 }
}

export default Home